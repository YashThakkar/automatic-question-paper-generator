
package questionpaper_generator;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
import java.sql.SQLException;

/**
 *
 * @author HOME
 */
public class MysqlConnect {
    Connection conn;
    
    public static Connection connectDB(){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/questionsdb","yash","yash2700");
         //   JOptionPane.showMessageDialog(null,"Connection established successfully");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"error while establishing connection ! "+e);
            return null;
        }
    } 
    
}
