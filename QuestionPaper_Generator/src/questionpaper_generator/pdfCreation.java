package questionpaper_generator;

import java.util.Set;
import java.util.HashSet;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Set;
import javax.swing.JOptionPane;
import questionpaper_generator.MysqlConnect;

public class pdfCreation {
    Connection conn = null;
    ResultSet rs = null;
    String FILE = "Questions.pdf";
    PreparedStatement ps = null;
    int total_marks;
    String subj = "";
    
    public pdfCreation(ArrayList<String> question ,int listsize,int tm,String sub) throws FileNotFoundException, DocumentException{
        int size = listsize;
        total_marks = tm;
        subj = sub;
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(FILE));
        document.open();
        System.out.println("pdf opened !!!!");
        addFormat(document,total_marks,subj);
        addContent(document,question,size);
        document.close();
        System.out.println("pdf opened !!!!");
    }
   
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    
    public static void addFormat(Document document, int total_marks,String subj)
            throws DocumentException {
        
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("                                                                                             subcode", catFont));
        if(subj.equals("java")){
            preface.add(new Paragraph("                                                                                             17515", catFont));
        }else if(subj.equals("sen")){
            preface.add(new Paragraph("                                                                                             17513", catFont));
        }
         addEmptyLine(preface, 1);
        // Lets write a big header
        if(total_marks == 20 || total_marks == 10){
            preface.add(new Paragraph("1 Hour / "+total_marks+" Marks", catFont));
        }
        else if(total_marks <=50){
            preface.add(new Paragraph("1.5 Hour / "+total_marks+" Marks", catFont));
        }else if(total_marks>50){
            preface.add(new Paragraph("3 Hours / "+total_marks+" Marks", catFont));
        }
            
        
        preface.add(new Paragraph(new Paragraph("_____________________________________________________________________________")));
        preface.add(new Paragraph("Instructions - (1) All Questions are Compulsory.",subFont));
        preface.add(new Paragraph("                        (2) Answer each next main Question on a new page.",subFont));
        preface.add(new Paragraph("                        (3) Illustrate your ansers with neat Sketches wherever "
                + "                                             necessary.",subFont));
        preface.add(new Paragraph("                        (4) Figures to the right indicate full marks.",subFont));
        preface.add(new Paragraph("                        (5) Assume suitable data, if necessary.",subFont));
        preface.add(new Paragraph("                        (6) Mobile Phone, pager and any other Electronic",subFont));
        preface.add(new Paragraph("                              communication devices are not permissable in",subFont));
        preface.add(new Paragraph("                              Examination Hall.",subFont));

        addEmptyLine(preface, 3);
        document.add(preface);
        // Start a new page
        //document.newPage();
    }
    
//    public void addContent(Document document,Set<String> question)throws DocumentException{
    public void addContent(Document document,ArrayList<String> question,int size)throws DocumentException{
        Paragraph subpara = new Paragraph();
        createTable(subpara,question,size);
        document.add(subpara);
    }
//    public void createTable(Paragraph subpara, Set<String> question)throws BadElementException{
    public void createTable(Paragraph subpara, ArrayList<String> question,int size)throws BadElementException{
        float a[] = {1,5,1};
        PdfPTable table = new PdfPTable(a);
        
        PdfPCell c1 = new PdfPCell(new Phrase("Question no."));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        
        c1 = new PdfPCell(new Phrase("Questions"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        
        c1 = new PdfPCell(new Phrase("Marks"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        
 
        System.out.println("size of question in pdf : "+question.size());
        int x = 0;
        int qno =1;
        while(x!=size){
            System.out.println("question ele : "+question.get(x));
//            table.addCell(question.get(x));
            table.addCell(Integer.toString(qno));
            qno++;
            x++;
            System.out.println("question ele : "+question.get(x));
            table.addCell(question.get(x));
            x++;
            System.out.println("question ele : "+question.get(x));
            table.addCell(question.get(x));
            x++;
        }
        subpara.add(table);
            
    }
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
